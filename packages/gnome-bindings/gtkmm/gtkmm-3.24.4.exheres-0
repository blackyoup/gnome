# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] meson

SUMMARY="C++ bindings for GTK+"
HOMEPAGE="https://www.gtkmm.org/"

LICENCES="LGPL-2.1"
SLOT="3"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    doc
    examples
    X
"

UPSTREAM_DOCUMENTATION="
    https://developer.gnome.org/gtkmm/stable/
        lang = en
        description = [ Reference manual ]
    ]]
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3.5]
        virtual/pkg-config[>=0.20]
        doc? (
            app-doc/doxygen
            dev-lang/perl:*[>=5.6.1]
            dev-libs/libxslt
            media-gfx/graphviz
        )
    build+run:
        dev-cpp/cairomm:1.0[>=1.12.0]
        dev-cpp/libsigc++:2[>=2.0.0]
        gnome-bindings/atkmm:1.6[>=2.24.2]
        gnome-bindings/glibmm:2.4[>=2.54.0]
        gnome-bindings/pangomm:1.4[>=2.38.2]
        x11-libs/gdk-pixbuf:2.0[>=2.35.5]
        x11-libs/gtk+:3[>=3.24.0][X?]
        examples? ( dev-libs/libepoxy[>=1.2] )
"

# Tests require X
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    # Doesn't build when disabled
    -Dbuild-deprecated-api=true
    -Dbuild-atkmm-api=true
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc build-documentation'
    'examples build-demos'
    'X build-x11-api'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dbuild-tests=true -Dbuild-tests=false'
)

src_prepare() {
    meson_src_prepare

    edo sed \
        -e "/install_docdir/s:/ book_name:/ '${PNVR}':" \
        -i docs/reference/meson.build
}

