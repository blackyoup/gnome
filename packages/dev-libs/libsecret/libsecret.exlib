# Copyright 2014 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ vala_dep=true with_opt=true ]
require meson
require test-dbus-daemon

SUMMARY="Secret Service dbus client library"
HOMEPAGE="https://wiki.gnome.org/Projects/Libsecret"

LICENCES="LGPL-2"
SLOT="1"
MYOPTIONS="debug gobject-introspection gtk-doc
    vapi [[ requires = gobject-introspection ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
        sys-devel/libtool
        virtual/pkg-config
        (
            app-text/docbook-xml-dtd:4.2
            app-text/docbook-xsl-stylesheets
            dev-libs/libxslt
        ) [[ description = [ For man pages ] ]]
        gtk-doc? ( dev-doc/gtk-doc[>=1.9] )
    build+run:
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libgcrypt[>=1.2.2]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.29] )
        !gnome-desktop/libsecret:0 [[
            description = [ slot move ]
            resolution = uninstall-blocked-after
        ]]
    test:
        dev-python/dbus-python
"

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'debug debugging'
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    vapi
)

src_prepare() {
    meson_src_prepare

    # test-vala-unstable segfaults, possible double-free?
    edo sed -e "/'test-vala-unstable',/d" -i "${MESON_SOURCE}"/libsecret/meson.build
}

src_test() {
    test-dbus-daemon_run-tests meson_src_test
}

