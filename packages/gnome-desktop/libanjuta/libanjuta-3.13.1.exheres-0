# Copyright 2011 Brett WItherspoon <spoonb@exherbo.org>
# Copyright 2015 Thomas Anderson <tanderson@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=anjuta
MY_PNV=${MY_PN}-${PV}
WORK="${WORKBASE}"/${MY_PNV}

require gnome.org [ pn=anjuta suffix=tar.xz ]
require gsettings gtk-icon-cache freedesktop-desktop freedesktop-mime

# FIXME: Fix me to build the whole IDE again; things have been severely broken ever since
# multiarch and likely before then; building libanjuta is a stopgap for some of its deps (gtkpod)
# which only need the library.
SUMMARY="A support library for the Anjuta integrated development environment (IDE)"
DESCRIPTION="
Anjuta DevStudio is a versatile Integrated Development Environment (IDE)
on GNOME Desktop Environment and features a number of advanced programming
facilities. These include project management, application and class wizards,
an on-board interactive debugger, powerful source editor, syntax highlighting,
intellisense autocompletions, symbol navigation, version controls, integrated
GUI designing and other tools.
"
HOMEPAGE="http://anjuta.org/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    debug
    glade             [[ description = [ Enable the glade ( GTK+ UI designer ) plugin ] ]]
    gobject-introspection
    gtk-doc
"

# FIXME gjsdir uses the wrong pkgconfig file and is automagic
DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.1]
        dev-util/pkg-config[>=0.22]
        gnome-desktop/gnome-doc-utils[>=0.18]
        sys-devel/gettext[>=0.12]
        gtk-doc? ( dev-doc/gtk-doc[>=1.4] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.5] )
    build+run:
        dev-lang/python:=
        dev-libs/glib:2[>=2.32.0]
        dev-libs/libgda:=[>=4.2.0]
        dev-libs/libxml2:2.0[>=2.4.23]
        dev-libs/vte:3.0[>=0.27.6]
        gnome-desktop/gdl:3.0[>=3.5.5]
        gnome-desktop/gtksourceview:3.0[>=3.0.0]
        sys-devel/autogen
        x11-libs/gdk-pixbuf:2.0[>=2.0.0]
        x11-libs/gtk+:3[>=3.4.0][gobject-introspection?]
        x11-libs/libXrender
        x11-libs/libXext
        glade? ( dev-util/glade[>=3.12.0] )
"

RESTRICT="test" # Docs fail validity check

# Can add these back as options once we build more than just libanjuta
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-plugin-devhelp
    --disable-plugin-glade
    --disable-plugin-subversion
    --disable-vala
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
    'glade plugin-glade'
    'gobject-introspection introspection'
    'gtk-doc'
)

# Only build libanjuta
src_compile() {
    edo cd libanjuta

    default
}

src_install() {
    edo cd libanjuta

    default
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    freedesktop-mime_pkg_postrm
    freedesktop-desktop_pkg_postrm
}
pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    freedesktop-mime_pkg_postinst
    freedesktop-desktop_pkg_postinst
}

